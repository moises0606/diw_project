Aqui puedes ir al proyecto en gitlab
    https://gitlab.com/moises0606/diw_project

En este proyecto he utilizado SASS

He utilizado HTML5

He hecho la web mas accessible, utilizando una herramienta como wave

La mayoria de la web está creada con grid

Esta misma es responsive. En cada directorio de scss (_header, _main, _footer) podemos
encontrar las media queryes para realizar-la responsive

He utilizado transiciones y animaciones con mixins, estos estan, en loader (_main.scss), tarifas,
y en el svg

Los mixins están en un fichero scss a parte

Los keyframes tambien estan en un fichero scss a parte

El svg lo podemos encontrar _svg.scss (el fichero esta importado en _main.scss)

También utilizo iconos unicodes en tarifas, importando en style.scss las librerias necesarias

He generado el favicon en todos los formatos que he encontrado y este se utilizará dependiendo
del dispositivo (S.O, tamaño)

También podemos encontrar una imagen, que utiliza el picture para seleccionar la resolucion de esta

Utilizo la libreria de font-awsome para tener los iconos de redes sociales

En los textos de .text--company limito el numero de caracteres para evitar que no sea

demasiada larga cada linea, al igual en estos, edito un poco el tipo de texto

En style.scss creo unas variables de sass, para acceder desde cualquier fichero

También utilizo un borde en tarifas, al igual que una sombra

En aboutus, podemos encontrar accesos directos, he utilizado el strong, abrebiaturas (abbr) y listado con números romanos, los accesos directos están utilizando flexbox, la página es responsive. Y finalmente los artículos están usando mixin de box-size.

Agregado a contacto el formulario con fieldset, también uso tablas, diferentes tipos de inputs, también hay un titulo con position absolute

He agregado en aboutus un target, la lista romana tiene un nth-child, index picture tiene clip path i un border radius

También está la opción de imprimir (se imprimirán las tarifas)